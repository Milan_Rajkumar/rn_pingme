import App from './src/App';
import {
  AppRegistry
} from 'react-native';

AppRegistry.registerComponent('ping_me', () => App);
