import React, { Component } from 'react';
import { Text, View, Image, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from './common';
import Icon from 'react-native-vector-icons/FontAwesome';

var dismissKeyboard = require('dismissKeyboard');

class LoginForm extends Component {
  onEmailChange (text) {
    this.props.emailChanged(text)
  }
  onPasswordChange (text) {
    this.props.passwordChanged(text)
  }
  onButtonPress () {
    const { email, password }  = this.props;
    this.props.loginUser(email, password, this.props.history);
  }
  signUp () {
    console.log('sign up is called!');
  }
  render () {
    const {
      formCenterStyle,
      imageStyle,
      imageStyle1,
      imageContainerStyle,
      formInputStyle,
      marginTopStyle,
      iconContainerStyle,
      linkTextContainerStyle,
      iconStyle,
      svStyle
    } = styles
    return (

      <TouchableWithoutFeedback onPress={dismissKeyboard}>
      <View style={styles.formContainerStyle}>
         <View style={imageContainerStyle}>
           <Image
             style={imageStyle}
             source={require('../assets/image/logo.png')}/>
         </View>

         <View style={formInputStyle}>
             <Input
               secureTextEntry = { false }
               label = "Email"
               placeholder = "user@gmail.com"
               onChangeText = { this.onEmailChange.bind(this) }
               value = { this.props.email }/>

             <Input
               secureTextEntry = { true }
               label = "Password"
               placeholder = "password"
               onChangeText = { this.onPasswordChange.bind(this) }
               value = { this.props.password }/>

           <Text style = { styles.errorTextStyle }>
             { this.props.error }
           </Text>
           <View style={marginTopStyle}>
             <Button onPress = { this.onButtonPress.bind(this)}>
               Login
             </Button>
           </View>
           <View style={linkTextContainerStyle}>
            <Text style={styles.linkTextStyle} onPress={this.signUp}>SignUp</Text>
            <Text style={styles.linkTextStyle} >Forget Password?</Text>
           </View>
           <View style={{alignItems: 'center', padding: 20}}>
            <Text>--------------------or--------------------</Text>
           </View>
           <View style={iconContainerStyle}>
             <Image
               style={iconStyle}
               source={require('../assets/image/facebook_logo.png')}/>
             <Image
               style={iconStyle}
               source={require('../assets/image/google_logo.png')}/>
             <Image
               style={iconStyle}
               source={require('../assets/image/linkedin_logo.png')}/>
               <Image
                 style={iconStyle}
                 source={require('../assets/image/twitter_logo.png')}/>
            </View>
         </View>
      </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = {
  errorTextStyle : {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  },
  formContainerStyle: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: 'white'
  },
  imageStyle: {
    width: 300,
    height: 200,
    resizeMode: 'contain'
  },
  imageStyle1: {
  },
  imageContainerStyle: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  formInputStyle: {
    flex: 4,
    padding: 5
  },
  marginTopStyle: {
    marginTop: 10
  },
  iconContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  iconStyle: {
    width: 36,
    height: 36,
    marginLeft: 15,
    marginRight: 15
  },
  linkTextContainerStyle: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'

  },
  linkTextStyle: {
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    color: 'black'
  }
}
const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading } = auth;
  return { email, password, error, loading };
}
export default LoginForm;
