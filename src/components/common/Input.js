import React from 'react';
import { TextInput, View, Text } from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Hideo } from 'react-native-textinput-effects';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
  const { containerStyle, inputStyle, labelStyle } = styles
  let iconName
  if (secureTextEntry) {
    iconName = 'lock';
  } else {
    iconName = 'user';
  }
  return (
    <View style= { containerStyle }>
      <Hideo
        iconClass={FontAwesomeIcon}
        iconName={iconName}
        iconColor={'white'}
        autoCorrect = { false }
        // this is used as backgroundColor of icon container view.
        iconBackgroundColor={'#3399ff'}
        inputStyle={{ color: '#464949' }}
        />
    </View>
  )
}

const styles = {
  containerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 2
  }
}

export { Input }
